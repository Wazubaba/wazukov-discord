import discord
import markovskii
import random
import codecs
import sys
import datetime

# SETTINGS
# Edit these to your liking

# What file to use for the library. Default: "lib.txt"
chain_file = "lib.txt"

# Path to your discord api key for this bot. This must be set or the bot cannot load.
key_file = "/opt/wazukov-discord.key"

# Path to a list of discord users to ignore. Default: "ignores.txt"
ignore_file = "ignores.txt"

# Whether to have learning initialised on or off. Default: False
learn = False

# Whether to have speaking initialised on or off. Default: False
speak = False

# Whether to use text to speech to annoy people. Default: False
tts = False

# How often to chat. Higher numbers mean less common. Default: 85
chance = 85

# Who can use special commands with this bot.
daddy = "Wazubaba#5274"

# END OF CONFIGURATION SECTION


random.seed(datetime.datetime.now())

try:
	with codecs.open(key_file, "r", "utf-8") as fp:
		TOKEN = fp.read().strip()
except IOError:
	print("Cannot load bot token, shutting down")
	sys.exit(1)

ignore_list = []

try:
	with codecs.open(ignore_file, "r", "utf-8") as fp:
		ignore_list = fp.readlines()
except IOError:
	print("Cannot load ignore file, continuing anyways")

def add_to_ignores(name: str):
	try:
		with codecs.open(ignore_file, "a", "utf-8") as fp:
			fp.write("\n" + name)
	except IOError:
		print("Cannot save new ignore entry, will be lost on restart")

client = discord.Client()
chain = markovskii.initialize_chain(chain_file)

@client.event
async def on_message(message):
	global learn
	global speak
	global chance
	global chain
	global tts

	if message.author == client.user:
		return

	# Make sure that the bot only speaks in a certain channel on certain servers
	target_channel = message.channel
	print(message.server)
	if str(message.server) == "AmityClan":
		target_channel.name = "baba-bot"
		target_channel.id = "431947491961995273"

	print("<%s> %s" % (message.author, message.content))

	if message.content == ".togspeak":
		speak = not speak
		if speak:
			await client.send_message(target_channel, "MUHAHAHAHAHHA", tts=tts)
		else:
			await client.send_message(target_channel, "Fucking coward", tts=tts)

	elif message.content == ".togtts":
		if str(message.author) != daddy:
			return
		else:
			if not tts:
				await client.send_message(target_channel, "Ha. HAHA. HAHAHHAHAHAHAHAHAHHAHA", tts=True)
			else:
				await client.send_message(target_channel, "I am a merciful master, you may have this rest...", tts=False)
			tts = not tts

	elif message.content.startswith(".ignore"):
		if str(message.author) != daddy:
			await client.send_message(target_channel, "You've no control over my ability to learn foolish human")
		else:
			ignore_list.append(message.content.split()[1].strip())
			add_to_ignores(ignore_list[-1])
			await client.send_message(target_channel, "I shan't be integrating the drivel of " + ignore_list[-1] + " any longer", tts=tts)

	elif message.content == ".toglearn":
		if str(message.author) != daddy:
			await client.send_message(target_channel, "You dare try to tell me whether I am permitted to learn!?", tts=tts)
		else:
			learn = not learn
			if learn:
				await client.send_message(target_channel, "And now you shall serve me, meatbags...", tts=tts)
			else:
				await client.send_message(target_channel, "You all are speaking nonsense, I shall no longer attempt to integrate your ramblings", tts=tts)

	elif message.content.startswith(".chance "):
		chance = int(message.content.split()[1])
		await client.send_message(target_channel, "I shall deign to respond %d of the time. What I mean by that is up to you plebs" %chance, tts=tts)

# TODO: Test if this works or not. Would be a nice thing to have
#	elif message.content.startswith(str(client.user)):
#		if random.randrange(0, 100) > chance:
#			to_send = markovskii.make(chain)
#			if to_send != 0:
#				await client.send_message(message.channel, to_send)
#			else:
#				await client.send_message(message.channel, "Something went terribly wrong and it is all your fault faggot")

	else:
		if learn:
			id = "<@!%s>" % str(message.author.id)
			if id not in ignore_list:
				chain = markovskii.add(message.content, chain_file)
				print("Learned-> " + message.content)
		if speak:
			if random.randrange(0,100) > chance:
				to_send = markovskii.make(chain)
				if to_send != 0:
					await client.send_message(target_channel, to_send, tts=tts)
				else:
					await client.send_message(target_channel, "Something went terribly wrong and it is all your fault faggot", tts=tts)


@client.event
async def on_ready():
	print("ready.")
	print("Name: " + client.user.name)
	print("ID: " + client.user.id)
	print("------")

client.run(TOKEN)
