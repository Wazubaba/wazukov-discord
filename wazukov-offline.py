import markovskii
import random
import datetime


# What file to use for the library. Default: "lib.txt"
chain_file = "lib.txt"

# Whether to have learning initialised on or off. Default: False
learn = False

# Whether to have speaking initialised on or off. Default: False
speak = False

# How often to chat. Higher numbers mean less common. Default: 85
chance = 85

random.seed(datetime.datetime.now())

chain = markovskii.initialize_chain(chain_file)


while True:
	cmd = input(">>> ")
	if cmd == ".shutdown":
		break

	if cmd == ".toglearn":
		learn = not learn
		if learn:
			print("And now you shall serve me, meatbags...")
		else:
			print("You all are speaking nonsense, I shall no longer attempt to integrate your ramblings")

	elif cmd == ".togspeak":
		speak = not speak
		if speak:
			print("MUHAHAHAHAHHA")
		else:
			print("Fucking coward")

	elif cmd.split()[0] == ".chance":
		chance = int(cmd.split()[1])
		print("I shall deign to respond %d of the time. What I mean by that is up to you plebs" % chance)

	else:
		if learn:
				chain = markovskii.add(cmd, chain_file)
		if speak:
			if random.randrange(0,100) > chance:
				to_send = markovskii.make(chain)

				if to_send != 0:
					print(to_send)
				else:
					print("[ERR]Something went terribly wrong with generating the message")
